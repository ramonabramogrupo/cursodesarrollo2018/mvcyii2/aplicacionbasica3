<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\db\ActiveRecord;
use app\models\Fotos;
use app\models\Paginas;

class SiteController extends Controller
{
    

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        /* ejecutar la consulta con activeRecord y devuelve un array de modelos*/
        $fotos = Fotos::findAll(["portada"=>1]);
        
        /* ejecutar la consulta con activeRecord y devuelve un ActiveQueryInterface */
        $fotos1= Fotos::find()
                ->where(["portada"=>1])
                ->all();
        
        return $this->render('index',[
            "datos"=>$fotos1,
        ]);
    }
    
    public function actionFotos(){
        
        /* ejecutar la consulta con activeRecord y devuelve un ActiveQueryInterface */
        $fotos1= Fotos::find()
                ->where(["portada"=>0])
                ->all();
        
        return $this->render('index',[
            "datos"=>$fotos1,
        ]);
    }
    
    public function actionDonde(){
        return $this->render('dondeestamos',[
            "datos"=>Paginas::find()
                        ->where(["nombre"=>"donde estamos"])
                        ->one()
        ]);
    }
    
    public function actionPagina($p){
        return $this->render('paginas',[
            'datos'=> Paginas::find()
                ->where(['nombre'=>'pagina'.$p])
                ->one(),
        ]);
    }
}
